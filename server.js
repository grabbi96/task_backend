const dotenv = require("dotenv");
dotenv.config({ path: "./config.env" });
const app = require("./app");
const mongoose = require("mongoose");
const http = require('http').Server(app);


// Server
const PORT = process.env.PORT;
const DB_Local = process.env.DB_LOCAL;
const DB_CLOUD = process.env.DB_CLOUD;

mongoose.connect(DB_CLOUD,
  {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
  });

mongoose.connection.on('connected', function () {
  console.log("mongodb connected successfully");
});
mongoose.connection.on('error', function (error) {
  console.log(error);
  console.error("mongodb connection interupted!");
});
mongoose.connection.on('disconnected', function () {
  console.error("mongodb disconnected !");
});
// const server = http.listen(PORT, () => {
//   console.log(`Server is on fire ${PORT}`);
// });
app.listen(PORT, () => {
  console.log(`Server is on fire ${PORT}`);
});