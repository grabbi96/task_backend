module.exports = {
    env: {
        es6: true,
        node: true,
    },
    extends: "eslint:recommended",
    globals: {
        Atomics: "readonly",
        SharedArrayBuffer: "readonly",
    },
    // parser: "babel-eslint",
    parserOptions: {
        ecmaVersion: 2018,
        sourceType: "module",
    },
    // plugins: [],
    rules: {
        "no-console": "off",
        // "no-unused-vars": ["error", { "vars": "all", "args": "after-used", "ignoreRestSiblings": false }],
        "no-unused-vars": "off",
        semi: [2, "always"]
    },
    settings: {},
};