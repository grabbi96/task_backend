const express = require("express");
const cors = require("cors");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const routes = require("./config/routes");
const AppError = require("./config/utils/appError");
const { globalError } = require("./config/utils/error");
const path = require("path");
const helmet = require("helmet");
const mongoose = require('mongoose');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');
// All configuration
const app = express();

app.use(helmet());
app.use(cors());
app.options('*', cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use("/public", express.static(path.join(__dirname, "public")));
if (process.env.NODE_ENV === "development") {
  app.use(morgan("dev"));
}

app.use((req, res, next) => {
  req.requestTime = new Date().toISOString();
  next();
});

// All Api
app.use("/task/api/v1", routes);

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));




app.use(globalError);

module.exports = app;
