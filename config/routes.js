const { Router } = require("express"),
  productRoute = require("../app/modules/product/routes/product.route");
const router = Router();


// user account
router.use("/product", productRoute);



module.exports = router;
