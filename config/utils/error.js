const sendErrorDev = (err, res) => {
  res.status(err.statusCode).json({
    status: err.status,
    error: err,
    errors: err.errors,
    message: err.message,
    stack: err.stack
  });
};

const sendErrorProd = (err, res) => {
  console.log(err);
  // Operational, trusted error: send message to client
  if (err.isOperational) {
    res.status(err.statusCode).json({
      status: err.status,
      message: err.message
    });

    // Programming or other unknown error: don't leak error details
  } else {
    // 1) Log error
    console.error("ERROR 💥", err);

    // 2) Send generic message
    res.status(500).json({
      status: "error",
      message: "Something went very wrong!"
    });
  }
};

module.exports = {
  catchError(res, error, code, message) {
    console.log(error);
    res.status(code).json({
      error: message
    });
  },
  globalError(err, req, res, next) {
    err.statusCode = err.statusCode || 500;
    err.status = err.status || "error";

    if (process.env.NODE_ENV === "development") {
      sendErrorDev(err, res);
    } else if (process.env.NODE_ENV === "production") {
      let error = { ...err };


      sendErrorProd(error, res);
    }
  }
};
