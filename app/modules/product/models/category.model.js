const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const common = {
    required: true,
    type: String,
    trim: true
};
const defaults = {
    type: String,
    default: ""
};
const CategorySchema = new Schema(
    {
        title: { ...defaults },
    },
    { timestamps: true }
);

const Category = mongoose.model("Category", CategorySchema);

module.exports = Category;
