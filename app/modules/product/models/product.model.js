const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const common = {
    required: true,
    type: String,
    trim: true
};
const defaults = {
    type: String,
    default: ""
};
const ProductSchema = new Schema(
    {

        title: { ...defaults },
        quantity: { ...defaults },
        price: Number,
        photo: { ...defaults },
        category: {
            type: mongoose.Schema.ObjectId,
            ref: 'Category'
        }

    },
    { timestamps: true }
);


const Product = mongoose.model("Product", ProductSchema);

module.exports = Product;
