const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const common = {
    required: true,
    type: String,
    trim: true
};
const defaults = {
    type: String,
    default: ""
};
const OrderSchema = new Schema(
    {
        items: [{
            type: mongoose.Schema.ObjectId,
            ref: 'Product'
        }],
        totalPrice: Number

    },
    { timestamps: true }
);


const Order = mongoose.model("Order", OrderSchema);

module.exports = Order;
