const Product = require("../models/product.model"),
    Category = require("../models/category.model"),
    catchAsync = require("../../../../config/utils/catchAsync"),
    AppError = require("../../../../config/utils/appError"),
    successResponse = require("../../../../config/response");



exports.create = catchAsync(async (req, res, next) => {

    let product = await Category.create(req.body);

    return successResponse(res, 200, {
        message: "Category created",
    });
});

exports.getAll = catchAsync(async (req, res, next) => {

    let categories = await Category.find();

    return successResponse(res, 200, {
        message: "Category created",
        categories
    });
});

exports.deletes = catchAsync(async (req, res, next) => {
    await Category.deleteMany();

    return successResponse(res, 200, {
        message: "Category created",
    });
});