


const Product = require("../models/product.model"),
    Order = require("../models/order.model"),
    catchAsync = require("../../../../config/utils/catchAsync"),
    AppError = require("../../../../config/utils/appError"),
    successResponse = require("../../../../config/response");



exports.create = catchAsync(async (req, res, next) => {

    let order = await Order.create(req.body);

    return successResponse(res, 200, {
        message: "Order created",
    });
});


exports.getAll = catchAsync(async (req, res, next) => {

    let order = await Order.find();

    return successResponse(res, 200, {
        message: "Order created",
        order
    });
});
