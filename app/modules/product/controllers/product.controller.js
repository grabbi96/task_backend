const Product = require("../models/product.model"),
    Category = require("../models/category.model"),
    catchAsync = require("../../../../config/utils/catchAsync"),
    AppError = require("../../../../config/utils/appError"),
    successResponse = require("../../../../config/response");



exports.create = catchAsync(async (req, res, next) => {

    let product = await Product.create(req.body);

    return successResponse(res, 200, {
        message: "Product created",
    });
});

exports.getAll = catchAsync(async (req, res, next) => {

    let products = await Product.find();

    return successResponse(res, 200, {
        message: "Category created",
        products
    });
});

exports.deletes = catchAsync(async (req, res, next) => {
    await Product.deleteMany();

    return successResponse(res, 200, {
        message: "Category created",
    });
});

exports.delete = catchAsync(async (req, res, next) => {
    await Product.findByIdAndDelete(req.body.id);

    return successResponse(res, 200, {
        message: "Category created",
    });
});