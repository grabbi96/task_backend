const router = require("express").Router(),
    ProductController = require("../controllers/product.controller"),
    OrderController = require("../controllers/order.controller"),
    CategoryController = require("../controllers/category.controller");

router.post("/category/create", CategoryController.create);
router.get("/categories", CategoryController.getAll);
router.delete("/categories", CategoryController.deletes);

router.post("/create", ProductController.create);
router.get("/", ProductController.getAll);
router.delete("/deletes", ProductController.deletes);
router.delete("/delete", ProductController.delete);

router.post("/order/create", OrderController.create);
router.get("/orders", OrderController.getAll);

module.exports = router;

